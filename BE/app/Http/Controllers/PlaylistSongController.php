<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Playlist_Songs;

class PlaylistSongController extends Controller
{
    //
    public function displaySongs(){
        return DB::table('playlist_songs')->get();
    }
        public function store(Request $request){

        $newPlaylistSong = new Playlist_Songs();
        $newPlaylistSong->song_id = $request->song_id;
        $newPlaylistSong->playlist_id = $request->playlist_id;
        $newPlaylistSong>save();
        return $newPlaylistSong;
        }
}
